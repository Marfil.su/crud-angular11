import { Comentario } from './../../models/comentario';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  text          : String   = "Titulo del card";
  autor         : String   = "Subtitulo del card";
  mostrar       : boolean  = true;
  nombreStorange: string   = "Jesus";
  personajes    : String[] = ["element 1","element 2","delement 3"];
  
  itemComentario: Comentario = new Comentario();

  comentarios: Comentario[] = [
    { id: 1, tipo: "comentario", comentario: "Primer comentario", app : "angular11" },
    { id: 2, tipo: "reporte_fallo", comentario: "Segundo comentario", app : "angular11" },
    { id: 3, tipo: "comentario", comentario: "Tercer comentario", app : "angular11" },
  ];
  constructor() { 
  
  }

  ngOnInit(): void {
  }

  addOrEdit(){
    console.log("===>addOrEdit");
    console.log(this.itemComentario);
    this.itemComentario.id = this.comentarios.length;
    this.comentarios.push( this.itemComentario );
    this.itemComentario = new Comentario();
  }
  
  openForEdit( _comentario:Comentario ){
    this.itemComentario = _comentario;
    if( this.itemComentario.id === 0 ){
      this.addOrEdit();
    }
  }

  delete(){
    if( confirm("Esta seguro que desea eliminar?")){
      this.comentarios = this.comentarios.filter(x => x != this.itemComentario);
      this.itemComentario = new Comentario();
    }
  }
}
