##New Component
ng generate component xyz

##Angular Material
ng add @angular/material

##Add PWA Support
ng add @angular/pwa

##Add Dependency
ng add _____

##Run and Watch Tests
ng test

##Build for Production
ng build --prod

##bootstrap
npm install bootstrap

##jQuery
npm install jquery

##popper.js
npm install popper.js

##font aweson
https://www.codegrepper.com/code-examples/typescript/angular-font-awesome+angular+11